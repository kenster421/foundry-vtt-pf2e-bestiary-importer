# Monster creator and compendium pack importer for Pathfinder 2

Provides APIs to create NPCs from JSON and import into compendium packs

This module uses trademarks and/or copyrights owned by Paizo Inc., which are used under Paizo's Community Use Policy. We are expressly prohibited from charging you to use or access this content. This module is not published, endorsed, or specifically approved by Paizo Inc. For more information about Paizo's Community Use Policy, please visit paizo.com/communityuse. For more information about Paizo Inc. and Paizo products, please visit paizo.com.

Forked from at https://gitlab.com/unindel/fvtt-pf2e-beastiary-1-compendium-pack-module

## API

This module provides two APIs:
* createPF2eMonsters(source_json_path)
* importPF2eMonsters(compendium_pack_collection_name)

### createPF2eMonsters(source_json_path)
This function will import all creatures in the json file specified as Actors in the world that is currently loaded

**source_json_path** _string_ path of file relative to the importJSON directory

**API Example usage**: `createPF2eMonsters('Extinction-Curse/xulgath-family_1-4.json')`

### importPF2eMonsters(compendium_pack_collection_name)
This function will import all actors in the world that is currently loaded into the provided compendium pack
To use this function, you should create a custom compendium in your world or import a system compendium to modify.

**compendium_pack_collection_name** _string_ name of custom world compendium pack

1. To find the compendium pack collection name, use the browser console and type `game.world.packs`
1. The **compendium_pack_collection_name** is a combination of your pack's `package` and the `name` concatenated by a dot
   1. Example `world.extinction-curse-bestiary`

**API Example usage**: `importPF2eMonsters('world.extinction-curse-bestiary')`

## License

Project Licensing:
* All Javascript in this project is licensed under the Apache License v2.


Content Usage and Licensing:
* Any Pathfinder Second Edition information used under the Paizo Inc. Community Use Policy (https://paizo.com/community/communityuse)
* Game system information and mechanics are licensed under the Open Game License (OPEN GAME LICENSE Version 1.0a).